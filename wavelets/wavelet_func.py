import numpy as np

def variance(X, win_size):

    rows = X.shape[0]
    #cols = X.shape[1]
    cols = 1

    if win_size % 2 != 1:
        raise ValueError("Window size must be uneven")

    hwin = (win_size - 1) / 2

    # http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
    for i in range(rows):
        for j in range(cols):
            # Could also consider compensation summation / Kahan summation
            # http://en.wikipedia.org/wiki/Compensated_summation
            # to reduce cancellation error

            # First pass
            N = 0
            S = 0
            for p in range(-hwin, hwin + 1):
                for q in range(-hwin, hwin + 1):
                    ii = i + p
                    jj = j + q

                    if ii < 0 or jj < 0 or ii >= rows or jj >= cols:
                        continue


                    N += 1
                    S += X[ii, jj]

            mu = S / N

            # Second pass
            SS = 0
            for p in range(-hwin, hwin + 1):
                for q in range(-hwin, hwin + 1):
                    ii = i + p
                    jj = j + q

                    if ii < 0 or jj < 0 or ii >= rows or jj >= cols:
                        continue

                    SS += (X[ii, jj] - mu) ** 2

            out[i, j] = SS / N

    return out

def _denoise_band(X, wavelet, levels, alpha=2):
    import numpy as np
    import pywt

    decomp = pywt.wavedec(X, wavelet, level=levels)
    for i, all_coeff in enumerate(decomp[1:]):
        minvar = np.empty(all_coeff[0].shape, dtype=float)
        minvar.fill(np.inf)
        # Handle horizontal, vertical and diagonal coefficients
        for coeff in all_coeff:
            for win_size in (3, 5, 7, 9):
                var = (coeff, win_size)
                mask = (var < minvar)
                minvar[mask] = var[mask]

            # Wiener estimator
            coeff *= (minvar / (minvar + alpha))

    rec = pywt.waverec(decomp, wavelet)
    rows, cols = X.shape
    if X.shape != rec.shape:
        rows_mod = rows % 2
        cols_mod = cols % 2
        return rec[rows_mod:, cols_mod:]
    else:
        return rec

def dwt_denoise(X, wavelet='bior3.1', levels=8, alpha=2):
    import numpy as np
    import pywt

    out = np.zeros(X.shape, dtype=float)
    if X.ndim == 3:
        bands = X.shape[2]

        for b in range(bands):
            out[:, :, b] = _denoise_band(X[..., b], wavelet, levels, alpha)
    else:
        out[:] = _denoise_band(X, wavelet, levels, alpha)

    return out